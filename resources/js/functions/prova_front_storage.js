$( document ).ready(function() {
	function retrievedata (){ 
		var nome      =   sessionStorage.getItem("nome");
		var email     =   sessionStorage.getItem("email")
		var assunto   =   sessionStorage.getItem("assunto")
		var tel       =   sessionStorage.getItem("telefone")
		var msg       =   sessionStorage.getItem("msg")

		document.querySelector("#nome").value = nome; 
		document.querySelector("#email").value = email;
		document.querySelector("#assunto").value = assunto;
		document.querySelector("#telefone").value = tel;
		document.querySelector("#mensagem").value = msg;
	}
	retrievedata();
	function savedata (){
		var nome      =   document.querySelector("#nome").value; 
		var email     =   document.querySelector("#email").value;
		var assunto   =   document.querySelector("#assunto").value;
		var tel       =   document.querySelector("#telefone").value;
		var msg       =   document.querySelector("#mensagem").value;
		
		sessionStorage.setItem('nome', nome);
		sessionStorage.setItem('email', email);
		sessionStorage.setItem('assunto', assunto);
		sessionStorage.setItem('telefone', tel);
		sessionStorage.setItem('msg', msg);
	}
	var myInterval = setInterval(savedata, 5000); 
});