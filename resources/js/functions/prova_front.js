$( document ).ready(function() {
    $('#nome').keyup(function () {
        $('.nome_r').text($(this).val());        
    });
    $('#email').keyup(function () {
        $('.email_r').text($(this).val());        
    });
    $('#assunto').keyup(function () {
        $('.assunto_r').text($(this).val());        
    });
    $('#telefone').keyup(function () {
        $('.telefone_r').text($(this).val());        
    });
    $('#mensagem').keyup(function () {
        $('.mensagem_r').text($(this).val());        
    });
});
